# Managed by idem-salt
from __future__ import absolute_import, print_function, unicode_literals
import sys

try:
    import asyncio
    import pop.hub

    HAS_POP = True
except ImportError:
    HAS_POP = False

__virtualname__ = "idem"
__func_alias__ = {"exec_": "exec"}


def __virtual__():
    if not HAS_POP:
        return False, "pop is not installed"
    if sys.version_info < (3, 6):
        return False, "idem only works on python3.6 and later"
    return True


def __init__(opts):
    if "idem.hub" not in __context__:
        # Initialize the hub
        hub = pop.hub.Hub()
        hub.pop.loop.create()
        # Load idem grains/states/exec modules onto the hub
        hub.pop.sub.add(dyne_name="acct")
        hub.pop.sub.add(dyne_name="config")
        hub.pop.sub.add(dyne_name="exec")
        hub.pop.sub.add(dyne_name="grains")
        hub.pop.sub.add(dyne_name="states")
        # Read the grains/idem config options
        hub.config.integrate.load(
            ["acct", "idem", "grains"], "idem", parse_cli=False, logs=False
        )

        # TODO Should grainsv2 be collected?
        # hub.grains.init.standalone()

        __context__["idem.hub"] = hub


def exec_(path, acct_file=None, acct_key=None, acct_profile="default", *args, **kwargs):
    """
    Call an idem execution module

    .. versionadd:: 3002

    path
        The idem path of the idem execution module to run

    acct_file
        Path to the acct file used in generating idem ctx parameters

    acct_key
        Key used to decrypt the acct file

    acct_profile
        Name of the profile to add to idem's ctx.acct parameter

    args
        Any positional arguments to pass to the idem exec function

    kwargs
        Any keyword arguments to pass to the idem exec function

    CLI Example:

    .. clod-block:: bash

        salt '*' idem.exec test.ping

    :maturity:      new
    :depends:       acct, pop, pop-config, idem
    :platform:      all
    """
    hub = __context__["idem.hub"]
    coro = hub.idem.ex.run(
        path,
        args,
        {k: v for k, v in kwargs.items() if not k.startswith("__")},
        acct_file=acct_file,
        acct_key=acct_key,
        acct_profile=acct_profile,
    )
    return hub.pop.Loop.run_until_complete(coro)
